Scripts for the actual execution of the analysis.

This directory is automatically added to `PATH` in the analysis environment, so any executables kept here will be available system wide.