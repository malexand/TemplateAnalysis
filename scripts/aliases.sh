#!/bin/bash

$ANALYSISUTILSROOT/scripts/aliases.sh
echo "alias interactive='run interactive.sh'"
echo "export TEMPLATEANALYSISROOT=$TEMPLATEANALYSISROOT"
if [ ! -z "$DAVINCIDEV_PROJECT_ROOT" ] ; then
    echo "export DAVINCIDEV_PROJECT_ROOT=$DAVINCIDEV_PROJECT_ROOT"
fi
if [ ! -z "$BENDERDEV_PROJECT_ROOT" ] ; then
    echo "export BENDERDEV_PROJECT_ROOT=$BENDERDEV_PROJECT_ROOT"
fi
for dname in workingdir datadir ; do
    python -c "from TemplateAnalysis.data import $dname
print 'export TEMPLATEANALYSIS' + '$dname'.upper() + '=' + $dname
"
done
echo "export GANGADIR=$TEMPLATEANALYSISDATADIR/gangadir"
echo "export GANGASTARTUP='$GANGASTARTUP;from TemplateAnalysis.Ganga import *'"
echo "export GANGAPYTHONPATH=$TEMPLATEANALYSISROOT/ganga/:$GANGAPYTHONPATH"
