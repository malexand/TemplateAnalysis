#!/bin/bash

name=$1
nameupper=$(python -c "print '$name'.upper()")
# Loop over files and replace all instances of TemplateAnalysis with the new name.
for f in $(find . -path ./.git -prune -o -type f) ; do
    sed -i "s/TemplateAnalysis/$name/g" $f
    sed -i "s/TEMPLATEANALYSIS/$nameupper/g" $f
done

# Rename the python and header directories
git mv python/TemplateAnalysis python/$name
git mv TemplateAnalysis $name
git mv ganga/TemplateAnalysis ganga/$name
git mv dict/TemplateAnalysisDict.h dict/${name}Dict.h
git mv dict/TemplateAnalysisDict.xml dict/${name}Dict.xml

srcurl=$(git remote -v | head -n 1 | awk '{print $2;}')
sed -i "s/PackageUrl=.*/PackageUrl=${srcurl}/" scripts/setup.sh

git checkout -- scripts/rename.sh
git checkout -- scripts/newproject.sh
git rm scripts/rename.sh
git rm scripts/newproject.sh
git commit -a -m "Rename TemplateAnalysis to $name"
git push origin master
