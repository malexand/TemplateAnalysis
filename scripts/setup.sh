#!/bin/bash

project=DaVinci
# Get the latest v4X series of DaVinci.
projectVersion=$(lb-run --list ${project} | grep v4 | head -n 1 | awk '{print $1;}')
PackageUrl=ssh://git@gitlab.cern.ch:7999/malexand/TemplateAnalysis.git
AnalysisUtilsUrl=ssh://git@gitlab.cern.ch:7999/malexand/AnalysisUtils.git

lb-dev ${project}/$projectVersion
cd ${project}Dev_$projectVersion
git clone "$AnalysisUtilsUrl"
git clone "$PackageUrl"
make
