#!/bin/bash

newpkgurl=$1
git clone --bare ssh://git@gitlab.cern.ch:7999/malexand/TemplateAnalysis.git
cd TemplateAnalysis.git
git push --mirror $newpkgurl
cd ..
rm -rf TemplateAnalysis.git

newname=$(basename $newpkgurl)
newname=${newname/\.git/}

git clone $newpkgurl
cd $newname
./scripts/rename.sh $newname
cd ..
./${newname}/scripts/setup.sh
rm -rf $newname
if [ -e newproject.sh ] ; then
    rm newproject.sh
fi
