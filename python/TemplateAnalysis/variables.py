'''Formulae for variables from the TTrees. The variable dicts are in the format
needed by makeroodataset.make_roodataset, for converting TTrees to RooDataSets.
These are also assigned as aliases when initialising TChains from the DataLibrary,
and given to the workspace instance so you can make RooRealVars easily with
workspace.roovar(varname).'''

variables = {'mass' : {'formula' : 'Xc_M',
                       'xmin' : 2300,
                       'xmax' : 2400,
                       'title' : 'Mass', 
                       'unit' : 'MeV', # optional
                       'discrete' : False, #optional, default False.
                       },
             }
