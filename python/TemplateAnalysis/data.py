'''Functions to access all the relevant datasets for the analysis, both TTrees and RooDataSets.'''

import os
from AnalysisUtils.data import DataLibrary
from TemplateAnalysis.variables import variables

# Directory where the data are kept.
datadir = os.environ.get('TEMPLATEANALYSISDATADIR', 
                         'root://eoslhcb.cern.ch//eos/lhcb/user/u/user/data/')

# Working dir, useful to define here so you can save script output there.
workingdir = '/path/to/work/dir'

'''All the TTree datasets, the tree names and file names (any number of file names can be given).
Each dataset must have a 'tree' and 'files' attribute. These are passed to 
AnalysisUtils.treeutils.make_chain, which makes a TChain. Datasets can also define:
- variables : the list of variables that will be put into the corresponding RooDataset,
    which will override the default list.
- datasetdir : the directory in which the corresponding RooDataset will be saved.
    By default this is the same directory as the input files, but if you're input
    files are on remote storage, it can be useful to set this to something local.
- aliases : a dict of name : subsitution pairs that will be applied to the TChain
    on initialisation using AnalysisUtils.treeutils.set_prefix_aliases.
- friends : a list of the names of datasets (also in the datapaths dict) that
    will be added to the TChain as friends on initialisation. Note that friend
    trees will automatically be constructed and added from directories under
    datalib.friends_directory(datasetname). If you want to make a TTree that will
    be automatically added as a friend, put it in a file of the name returned by
    datalib.friend_file_name(datasetname, friendname, treename).
- selection : a string selection that will be applied when making the RooDataset.
'''
datapaths = {'SomeData' : {'tree' : 'DecayTree',
                           'files' : [os.path.join(datadir, 'ntuple.root')]}
             }

# This is the default list of variable names that will be added to the RooDataset
# for each dataset. The names correspond to variables defined in the variables module.
varnames = ('mass',)

'''Make the DataLibrary. See AnalysisUtils.data.DataLibrary for full functionality.
You can get the TChain with, eg,
tree = datalib.SomeData()
or
tree = datalib.get_data('SomeData')

Get the RooDataset with
dataset = datalib.SomeData_Dataset()
or
dataset = datalib.get_dataset('SomeData')

Get the info on a dataset with
info = datalib.get_data_info('SomeData')

Get the name of a file that will be automatically added as a friend if it exists:
fname = datalib.friend_file_name('SomeData', 'SomeDataFriend', 'FriendTree', makedir = True)
makedir = True means the directory for the file will be made if it doesn't exist (it must be
a local directory). If you then make the file containing a TTree named 'FriendTree' it
will automatically be added as a friend when 'SomeData' is retrieved.
'''

datalib = DataLibrary(datapaths, variables, varnames)
