'''Functions for building fit PDFs. Everything is built via the workspace.'''

import ROOT
from TemplateAnalysis.workspace import workspace, get_variable, get_component
