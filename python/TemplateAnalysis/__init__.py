
def load_cpp_lib() :
    '''Load the C++ libary for this package into ROOT.'''
    import ROOT
    ROOT.gSystem.Load('libTemplateAnalysisLib.so')
    ROOT.gSystem.Load('libTemplateAnalysisDict.so')

# Uncomment the below if you want to automatically load your C++ libraries
# load_cpp_lib()
