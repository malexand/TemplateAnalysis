'''Instantiates the RooWorkspace for the current analysis. The Workspace class from
AnalysisUtils.workspace has various functions to make usage easier, particularly
the Workspace.roovar and Workspace.factory functions.'''

from TemplateAnalysis.data import datadir
from AnalysisUtils.workspace import *
import os
from TemplateAnalysis.variables import variables

# If you want a persistent workspace between sessions you can give it a file name,
# but obviously this persists mistakes as well.
#workspacefilename = os.path.join(datadir, 'workspace.root')
workspace = Workspace('workspace', 
                      #fname = workspacefilename,
                      variables = variables
                      )
