# Template analysis structure in the LHCb software structure

The code is organised as an LHCb CMake project, so you can build & run it in an LHCb application. It requires [AnalysisUtils](https://gitlab.cern.ch/malexand/AnalysisUtils).

# Making a new analysis from the template.

To make a new analysis, firstly, [make a new, empty project](https://gitlab.cern.ch/projects/new). Then copy its clone URL and do

`wget https://gitlab.cern.ch/malexand/TemplateAnalysis/raw/master/scripts/newproject.sh`

`sh newproject.sh <packageurl>`

This will:
- Clone TemplateAnalysis into your new project.
- Check out your clone and rename it to the name of your new analysis.
- Commit & push the changes.
- Delete that working copy.
- Make a DaVinciDev directory containing your new analysis package and AnalysisUtils.
- Build it.

Then you can get on with analysis!

# Building

After following the steps above, to build this package and AnalysisUtils in a new location, execute the code in [`scripts/setup.sh`](scripts/setup.sh).

Then `./run <whatever>` will execute whatever command in the analysis environment, so you can use any of the analysis functionality.

You can set some handy aliases by doing

`eval $(./run aliases.sh)`

This will define a `run` aliases, which points to the absolute path of the `./run` executable, so you can use it from any directory. It also defines an `interactive` aliases, which executes `scripts/interactive.py` (also available from any directory), so you can start ipython with some analysis functionality imported.

# Directory structure

## options

Python LFN datasets & ntupling options.

## python/TemplateAnalysis

Shared python modules for core functionality, eg, accessing ntuples/RooDataSets, building fit models, plotting utils.

## scripts

Scripts containing main code for the analysis. These should be fairly minimal with all the functionality living in `python/TemplateAnalysis`. This directory is automatically added to `PATH` in the analysis environment, so any executables in this directory will be available system wide.

## tmva

Where to keep the output of TMVA trainings (.root and .xml files).

## src

Home of C++ source code. Headers should live in `TemplateAnalysis/TemplateAnalysis`. To build your C++ and link it so it can be loaded in python, uncomment the relevant lines in `CMakeLists.txt`, then add the classes you want to be able to load to `dict/TemplateAnalysis.h` and `dict/TemplateAnalysis.xml`. Then you can use `load_cpp_lib` from `python/TemplateAnalysis/__init__.py` to load your C++ classes, or you can call it in `python/TemplateAnalysis/__init__.py` so they're loaded automatically any time you `import TemplateAnalysis`.
